require 'rubygems'
require 'bundler/setup'

gem 'albacore'
require 'albacore'
require 'rake/clean'

PORT = "34444"

dirs = {
    :build          => File.expand_path("build"),
    :orchard        => File.expand_path("src/orchards/Orchard").gsub("/", "\\"),
    :dependencies   => File.expand_path("build/dependencies"),
    :libs           => File.expand_path("libs"),
    :src            => File.expand_path("src"),
    :orchardWeb     => File.expand_path("Orchard.Web").gsub("/", "\\"),
    :tools          => File.expand_path("tools"),
    :buildtools     => File.expand_path("tools/build")
}

modules = ['/modules/Four2n.BetterMarkdown',]
themes = []

CLEAN.clear()
CLEAN.include(dirs[:build] + '/*')
CLEAN.exclude('**/.gitignore')

task :default => :build

msbuild :build do |msb|
    msb.targets :Clean, :Build
    msb.verbosity = "minimal"
    msb.solution = "#{dirs[:src]}/OrchardWorkshop.sln"
end

unzip :extractOrchardDependency, :orchardVersion do |unzip, args|
    unzip.destination = dirs[:dependencies]
    unzip.file = File.join(dirs[:libs] + "/Orchard/Orchard.Web.#{args.orchardVersion}.zip")
end

task :copyOrchardDependency, [:orchardVersion] => [:extractOrchardDependency] do |t, args|
    sh "xcopy #{dirs[:dependencies].gsub("/", "\\")}\\Orchard\\*.* #{(dirs[:orchard]).gsub("/", "\\")}_#{args.orchardVersion}\\ /Y /E /Q /EXCLUDE:#{File.join(dirs[:buildtools] + "/CopyDependencies.excluded").gsub("/", "\\")}"
end

task :copyOrchardLocalizationDependency, :orchardVersion do |t, args|
    sh "xcopy #{dirs[:libs].gsub("/", "\\")}\\OrchardLocalizations\\*.* #{dirs[:orchard].gsub("/", "\\")}_#{args.orchardVersion}\\ /Y /E /Q /EXCLUDE:#{File.join(dirs[:buildtools] + "/CopyDependencies.excluded").gsub("/", "\\")}"
end

task :linkOrchardModules, :orchardVersion do |t, args|
    modules.each do |item|
        link = dirs[:orchard] + "_#{args.orchardVersion}\\Modules\\" + item.split("/").last
        target = (dirs[:src] + item).gsub("/", "\\");
        if Dir.exist?(link) == false
            sh "cmd /C mklink /D #{link} #{target}"
        end
    end
end

task :linkOrchardThemes, :orchardVersion do |t, args|
    themes.each do |item|
        link = dirs[:orchard] + "_#{args.orchardVersion}\\Themes\\" + item.split("/").last
        target = (dirs[:src] + item).gsub("/", "\\");
        if Dir.exist?(link) == false
            sh "cmd /C mklink /D #{link} #{target}"
        end
    end
end

task :linkOrchardWeb, :orchardVersion do |t, args|
    link = dirs[:orchard] + "_#{args.orchardVersion}"
    target = dirs[:orchardWeb].gsub("/", "\\");
    if Dir.exist?(target) == true
        sh "rmdir #{target}"
    end
    sh "cmd /C mklink /D #{target} #{link}"
end

desc "Preparing development environment"
task :prepareDevelopment, [:orchardVersion] => [:clean, :copyOrchardDependency, :linkOrchardModules, :linkOrchardThemes, :linkOrchardWeb] do |t, args|
end

desc "Starts Orchard application through IISExpress"
exec :runIISExpress do |cmd|
    cmd.command = "iisexpress"
    cmd.parameters = "/path:#{dirs[:orchardWeb]}", "/port:" + PORT

end 

task :nugetModules do
	orchardPath = dirs[:orchard] + "\\bin\\"
	modules = FileList.new(dirs[:src] + "/app/*Syzyf.OrchardModule*")
    modules.each do |item|
		moduleName = item.split("/").last
		sh "cd #{orchardPath}"
		sh "Orchard.exe package create #{moduleName} c:\\temp"
	end
end

task :nugetThemes do
	orchardPath = dirs[:orchard] + "\\bin\\"
	themes = FileList.new(dirs[:src] + "/app/*OrchardTheme*")
    themes.each do |item|
		themeName = item.split("/").last
		sh "cd #{orchardPath}"
		sh "Orchard.exe package create #{moduleName} c:\\temp"
	end
end

desc "Create nugget packages for modules"
task :createNugetPackages => [:nugetModules, :nugetThemes]
