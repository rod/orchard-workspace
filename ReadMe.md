# Preparing environment to development #

1. Install ruby 1.9.3.
2. Install bundler: `gem install bundler`.
3. Install required gems" `bundle install` in project directory.
4. Run `rake prepareDevelopment` with administrative preferences.
